## SIMDAVIS App: a dataset and data visualisation tool for Ln-based SIMs

**SIMDAVIS Version 1.2.1**  
*Last Updated February 18th, 2025*

This repository contains the Free Libre Open-Source Software (FLOSS) that is the basis for the SIMDAVIS App. The App is written in R language and uses the Shiny package. If you wish to run this software in your computer, you must have [R installed](https://www.r-project.org/), and also the following R packages: readr, dplyr, shiny, ggplot2 and DT. You can access the SIMDAVIS web App directly via this link: <https://go.uv.es/rosaleny/SIMDAVIS>

SIMDAVIS was specifically developed to accompany a publication, and we kindly request its citation if you use it: Duan, Y., Rosaleny, L.E., Coutinho, J.T. *et al*. Data-driven design of molecular nanomagnets. *Nat Commun* **13**, 7626 (2022). <https://www.nature.com/articles/s41467-022-35336-9>

**OUTLINE**  
SIMDAVIS (for **S**ingle **I**on **M**agnet **DA**ta **VIS**ualisation) is a dashboard-style web application, designed to fulfill a double objective. First, it is a dataset of Lanthanide-based Single Ion Magnets, which as of December 2022 contains 1411 samples from 448 articles published between 2003 and 2019. The dataset can be downloaded via the Data Tab \> Download Data. SIMDAVIS is also a visualisation tool, allowing the production of scatter plots, box plots and bar charts, which then can be downloaded as vectorial PDF files. You can also download the represented subset of the data as a tsv file.

In the **ScatterPlots tab** the user can represent 15 quantitative physical properties versus one another; the chemical category (plus publication year) of every data point is identified by a color. For each of the 12 qualitative categorization possibilities, each category may be shown or hidden; checking a box adds a linear regression for each category. Click on any data point in the plot to show its sample ID, compound and DOI hyperlink in the table below the plot.

The **BoxPlots tab** allows to examine the distribution of each SIMs quantitative property vs a categorization criterion. The **BarCharts tab** explores the frequency of different qualitative variables in our dataset. The **Data tab** to browse the dataset allows choosing the data columns to show, ordering in ascending or descending order, and filtering by arbitrary keywords.\
![](www/fig_Home_SIMDAVIS.png)

**CHANGELOG**

**Update (February 18th, 2025) to version 1.2.1**   
Fix some incorrect assignments (Other Families to P=O).

**Update (June 12th, 2024) to version 1.2.0**   
Used new simdavis dataset with two new families (Crown Ether & P=O), and reassignments in the Mixed Ligands and Other Families.  

**Update (December 13th, 2022) to version 1.1.10**  
Added link to published Nature Communications paper: Duan, Y., Rosaleny, L.E., Coutinho, J.T. *et al*. Data-driven design of molecular nanomagnets. *Nat Commun* **13**, 7626 (2022). <https://www.nature.com/articles/s41467-022-35336-9>

**Update (July 22nd, 2022) to version 1.1.9**  
Released SIMDAVIS version 1.1.9.

**Update (July 14th, 2022) to version 1.1.8.9006**  
Fixed one sample data, display closest_polyhedron with 45º in boxplots, reordered levels of closest_polyhedron and removed minor grid marks for axes (log10).

**Update (July 5th, 2022) to version 1.1.8.9005**  
Workaround for bug in annotation_logticks, removed discrete variable pyCF_coordination_number from plots, changed C_Raman and n_Raman to C and n, removed data from pyCrystalField, updated variables_verbose.txt.

**Update (July 1st, 2022) to version 1.1.8.9004**    
Fixed some data in dataset, added CCDC_COD_ID, link_to_CIF and CIF_type variables, added pyCrystalField and SHAPE calculations and calculated slope_CSM and axial_distorsion.

**Update (February 14th, 2022) to version 1.1.8.9003**    
Added new variables available for plots (C_Raman, n_Raman, tau_qtm) and updated About SIMDAVIS \> Variables tab with the newest additions.

**Update (February 2nd, 2022) to version 1.1.8.9002**  
Added new data calculated via pyCrystalField and SHAPE, new variables available for plots (closest_polyhedron, main_MJ, main_MJ_module), partial revision of CIF assignments to samples and link to CIFs. Added relaxation data (C_Raman, n_Raman, tau_qtm).

**Update (August 3rd, 2021) to version 1.1.8.9001**    
Added filters to columns in View Data tab and added data calculated via pyCrystalField and SHAPE.

**Update (July 26, 2021) to version 1.1.8**  
Added meta tags to make SIMDAVIS App searchable.

**Update (July 12, 2021) to version 1.1.7**  
Added record T_hyst samples from 2018, year also as numerical variable, descriptions of plots with number of annotated samples, download buttons for the data feeding each plot, only present categories are shown in z checkbox, and updated Acknowledgements.

**Update (May 3, 2021) to version 1.1.6**  
Fixed some chemical family assignments.

**Update (April 19, 2021) to version 1.1.5**  
Fixed some X''\_max data, updated README, and made the bitbucket repository public.

**Update (April 16, 2021) to version 1.1.4**  
Fixed some Hyst data, added AGPL-3.0 license tab and updated README.md

**Update (April 13, 2021) to version 1.1.3**  
Added new clustering with more categories for download, assigned "Unassigned" to NA in mag_struct_cluster, removed unused levels in coord. number and number of ligands, created different dataset for download with raw URLs, added descriptions for the clustering variables in the Variables tab, and fixed some errors.

**Update (March 29, 2021) to version 1.1.2**    
Added the updated molecular and magnetostructural clustering data, and renamed chemical families.

**Update (March 26, 2021) to version 1.1.1**  
A new home tab and new subtabs in "About SIMDAVIS" tab (Authors, Feedback&Bugs, Changelog) added.

**Update (March 22, 2021) to version 1.1**  
Added CIF links and missing magnetic data from 2016, fixed errors in T_B3/T_B3H values when field was present, and some other minor corrections.

**Update (March 18, 2021) to version 1.0.3**  
DOI opens in new window, removed values H = 0, and some corrections.

**Update (March 16, 2021) to version 1.0.2**  
Histogram to bar chart, variable names and factors changed.

**Update (March 11, 2021) to version 1.0.1**  
Fixed some compound names, the chemical families of some samples were reassigned, preselected variables in data tab changed and a tab about the SIMDAVIS app is added.

**Update (March 8, 2021) to version 1.0**  
Fixed one doi error and family categories.

Update (December 3, 2019) to version 0.9

Update (July 26, 2019) to version 0.8

Update (July 8, 2019) to version 0.7

Update (July 2, 2019) to version 0.6

Update (June 26, 2019) to version 0.5

Update (June 21, 2019) to version 0.4

Update (June 14, 2019) to version 0.3

Update (June 6, 2019) to version 0.2

Update (May 31, 2019) to version 0.1
