READ ME

The files inside this folder "dataForPlots_pBits" contain all the data required to prepare the plots shown Figure 5 (a) and  Figure 5 (b) in the main text of the article "Lanthanide molecular nanomagnets as probabilistic bits".

The figures in the Supplementary Information Sections 3 and 4 from the same article can also be reproduced with the data contained in the files data_figsSupplementaryInformationSection3.csv and data_figsSupplementaryInformationSection4.csv.


